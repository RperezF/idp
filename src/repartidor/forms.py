from django import forms
from .models import Repartidor


class ContactForm(forms.Form):
    nombre = forms.CharField(required=False)
    email = forms.EmailField()
    mensaje = forms.CharField(widget=forms.Textarea)


class RepartidorModelForm(forms.ModelForm):

    class Meta:
        model = Repartidor
        fields = ["nombre", "rut", "fono", "mail", "genero", "tipo_de_licencia"]

    def clean_nombre(self):
        nombre = self.cleaned_data.get("nombre")

        if len(nombre) < 5:
            raise forms.ValidationError("El nombre no puede ser muy corto")
        elif len(nombre) > 80:
            raise forms.ValidationError("El nombre no puede ser muy largo")
        return nombre

    def clean_fono(self):
        fono = self.cleaned_data.get("fono")

        if len(fono) < 1:
            raise forms.ValidationError("Fono invalido")
        elif len(fono) > 20:
            raise forms.ValidationError("El fono no puede ser tan largo")
        return fono
