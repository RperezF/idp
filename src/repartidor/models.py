from django.db import models

# Create your models here.

class Repartidor(models.Model):
    id_repartidor = models.AutoField(primary_key=True, unique=True)
    nombre = models.CharField(max_length=50, blank=False, null=False)
    rut = models.CharField(max_length=12, blank=False, null=False)
    fono = models.CharField(max_length=20)
    mail = models.EmailField(max_length=75, blank=True, null=True)
    genero = models.CharField(max_length=10, blank=False, null=True, choices=(('M', 'Masculino'), ('F', 'Femenino')))
    tipo_de_licencia = models.CharField(max_length=8, blank=False, null=True, choices=(('A', 'A'), ('B', 'B'), ('No tiene', 'No tiene')))

    def __str__(self):
        return self.nombre
