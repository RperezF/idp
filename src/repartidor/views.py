from django.shortcuts import render
from .forms import RepartidorModelForm, ContactForm
from .models import Repartidor
from django.conf import settings
from django.core.mail import send_mail
# Create your views here.


def agregar_repartidor(request):
     form = RepartidorModelForm(request.POST or None)
     mensaje = "datos guardados correctamente"
     if request.user.is_authenticated():
         titulo = "BIENVENIDO %s" % (request.user)

     contexto = {
         "el_titulo": titulo,
         "el_formulario": form,
     }

     if form.is_valid():
         instance = form.save(commit=False)
         if not instance.mail:
             instance.mail = "SIN DATOS"
         instance.save()

         contexto = {
            "el_formulario": form,
            "mensaje": mensaje,
         }

         print(instance)

     return render(request, "agregar_repartidor.html", contexto)


def contacto(request):
    form = ContactForm(request.POST or None)

    if form.is_valid():
        formulario_nombre = form.cleaned_data.get("nombre")
        formulario_email = form.cleaned_data.get("email")
        formulario_mensaje = form.cleaned_data.get("mensaje")
        asunto = "Formulario de contacto"
        email_from = settings.EMAIL_HOST_USER
        email_to = [email_from, "jorgeborquez@udec.cl"]
        email_mensaje = "Enviado por %s - Correo: %s - Mensaje: %s" %(formulario_nombre, formulario_email, formulario_mensaje)
        send_mail(asunto, email_mensaje, email_from, email_to, fail_silently=False)

    if request.user.is_authenticated():
        titulo = "BIENVENIDO %s" % (request.user)

    contexto = {
        "el_titulo": titulo,
        "el_contacto": form,
    }

    return render(request, "contacto.html", contexto)