from django.contrib import admin
from .models import Repartidor
from .forms import RepartidorModelForm

class AdminRepartidor(admin.ModelAdmin):
    list_display = ["id_repartidor", "nombre", "rut", "fono"]
    list_filter = ["id_repartidor", "nombre"]
    search_fields = ["nombre", "rut"]
    form = RepartidorModelForm


admin.site.register(Repartidor, AdminRepartidor)
