# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-05-15 22:02
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('repartidor', '0003_auto_20180515_1800'),
    ]

    operations = [
        migrations.AlterField(
            model_name='repartidor',
            name='mail',
            field=models.EmailField(blank=True, max_length=75, null=True),
        ),
        migrations.AlterField(
            model_name='repartidor',
            name='nombre',
            field=models.CharField(max_length=50),
        ),
    ]
