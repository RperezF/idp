# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-05-15 22:05
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('repartidor', '0004_auto_20180515_1802'),
    ]

    operations = [
        migrations.AlterField(
            model_name='repartidor',
            name='mail',
            field=models.EmailField(max_length=75, null=True),
        ),
    ]
