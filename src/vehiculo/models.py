from django.db import models

# Create your models here.

class Vehiculo(models.Model):
    id_vehiculo = models.AutoField(primary_key=True, unique=True)
    tipo_vehiculo = models.CharField(max_length=50, blank=False, null=False, choices=(('auto', 'auto'), ('motito', 'motito')))
    marca = models.CharField(max_length=50, blank=False, null=False)
    modelo = models.CharField(max_length=50, blank=False, null=False)
    año = models.IntegerField(default=2012)
    cilindrada = models.IntegerField()
    patente = models.CharField(max_length=10, blank=False, null=False)
    repartidor_a_cargo = models.ForeignKey('repartidor.Repartidor')

    def __int__(self):
        return self.id_vehiculo
