from django import forms
from .models import Vehiculo


class VehiculoModelForm(forms.ModelForm):

    class Meta:
        model = Vehiculo
        fields = ["tipo_vehiculo", "marca", "modelo", "año", "cilindrada", "patente", "repartidor_a_cargo"]


    def clean_marca(self):
        marca = self.cleaned_data.get("marca")

        if len(marca) < 5:
            raise forms.ValidationError("El nombre no puede ser muy corto")
        elif len(marca) > 80:
            raise forms.ValidationError("El nombre no puede ser muy largo")
        return marca

    def clean_modelo(self):
        modelo = self.cleaned_data.get("modelo")

        if len(modelo) < 5:
            raise forms.ValidationError("El nombre no puede ser muy corto")
        elif len(modelo) > 80:
            raise forms.ValidationError("El nombre no puede ser muy largo")
        return modelo

    def clean_año(self):
        año = self.cleaned_data.get("año")
        if año:
            if año < 2012:
                raise forms.ValidationError("El año no puede ser de un inferior a 2012")
            return año
        else:
            raise forms.ValidationError("Este campo es requerido")

