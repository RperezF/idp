from django.contrib import admin
from .models import Vehiculo
from .forms import VehiculoModelForm

class AdminVehiculo(admin.ModelAdmin):
    list_display = ["id_vehiculo", "tipo_vehiculo", "marca", "modelo", "patente", "repartidor_a_cargo"]
    list_filter = ["id_vehiculo", "marca", "modelo", "patente"]
    search_fields = ["marca", "modelo", "patente"]
    form = VehiculoModelForm


admin.site.register(Vehiculo, AdminVehiculo)