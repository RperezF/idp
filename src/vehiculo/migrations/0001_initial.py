# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-05-15 18:03
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('repartidor', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Vehiculo',
            fields=[
                ('id_vehiculo', models.AutoField(primary_key=True, serialize=False, unique=True)),
                ('tipo_vehiculo', models.CharField(max_length=50)),
                ('marca', models.CharField(max_length=50)),
                ('modelo', models.CharField(max_length=50)),
                ('cilindrada', models.IntegerField()),
                ('patente', models.CharField(max_length=10)),
                ('repartidor_a_cargo', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='repartidor.Repartidor')),
            ],
        ),
    ]
