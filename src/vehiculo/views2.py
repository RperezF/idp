from django.shortcuts import render
from .forms import VehiculoModelForm
from .models import Vehiculo
# Create your views here.

def agregar_vehiculo(request):
    form = VehiculoModelForm(request.POST or None)
    if request.user.is_authenticated():
        titulo = "BIENVENIDO %s" % (request.user)

    contexto = {
        "el_titulo": titulo,
        "el_formulario": form,
    }

    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()

        contexto = {
            "el_formulario": form,
        }

    return render(request, "agregar_vehiculo.html", contexto)
